---
layout: post
title: "NoNameCon CTF Quals 2018 - Write-up"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
date: 2018/04/23
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : NoNameCon CTF Quals 2018
- **Website** : [ctf.nonamecon.org](https://ctf.nonamecon.org/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/616/)

## 50 - Subdomain

> We want you to find hidden subdomain `*.nonameconctf2018.xyz` and get the flag from it. Don't attack DNS servers

I used `sublist3r` and dorking to find some subdomains:

```
$ sublist3r -d nonameconctf2018.xyz 

                 ____        _     _ _     _   _____
                / ___| _   _| |__ | (_)___| |_|___ / _ __
                \___ \| | | | '_ \| | / __| __| |_ \| '__|
                 ___) | |_| | |_) | | \__ \ |_ ___) | |
                |____/ \__,_|_.__/|_|_|___/\__|____/|_|

                # Coded By Ahmed Aboul-Ela - @aboul3la
    
[-] Enumerating subdomains now for nonameconctf2018.xyz
[-] Searching now in Baidu..
[-] Searching now in Yahoo..
[-] Searching now in Google..
[-] Searching now in Bing..
[-] Searching now in Ask..
[-] Searching now in Netcraft..
[-] Searching now in DNSdumpster..
[-] Searching now in Virustotal..
[-] Searching now in ThreatCrowd..
[-] Searching now in SSL Certificates..
[-] Searching now in PassiveDNS..
[-] Total Unique Subdomains Found: 7
www.nonameconctf2018.xyz
bank.nonameconctf2018.xyz
chat.nonameconctf2018.xyz
flag.nonameconctf2018.xyz
hidden.nonameconctf2018.xyz
info.nonameconctf2018.xyz
interview.nonameconctf2018.xyz
```

Then as no interesting DNS entries were available I checked if a web server was available:

```
$ aria2c http://hidden.nonameconctf2018.xyz/ -o output && cat output

04/23 15:44:44 [NOTICE] Downloading 1 item(s)

04/23 15:44:44 [NOTICE] Download complete: /root/output

Download Results:
gid   |stat|avg speed  |path/URI
======+====+===========+=======================================================
1e53ff|OK  |   3.1KiB/s|/root/output

Status Legend:
(OK):download completed.
That was easy. Flag for 50 points: nnc{ef62e674aa6585412e1ee529b4bd9090}
```

Flag was: `nnc{ef62e674aa6585412e1ee529b4bd9090}`.
