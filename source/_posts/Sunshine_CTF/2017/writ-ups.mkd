---
layout: post
title: "Sunshine CTF 2017 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - crypto
  - web
  - forensics
date: 2017/04/10
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : Sunshine CTF 2017
- **Website** : [sunshinectf.org](https://sunshinectf.org/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/449/)

## 1 - Snowflake - Crypto

`sun{74K3_Y0UR_p4r7icIP47i0N_7r0PhY_y0U_5N0Wfl4k3}`

## 25 - Easy 1 - Web

> Doom is upon us. We are surrounded by enemies on all sides. There is only one hope. Use your hacking skills to find it.
>
> easy1.web.sunshinectf.org

```
$ curl --head http://easy1.web.sunshinectf.org/
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 07 Apr 2017 21:59:44 GMT
Content-Type: text/html
Connection: keep-alive
X-Powered-By: PHP/5.5.9-1ubuntu4.11
flag: sun{k4rEfUL_D0nT_H1T_y0uR_HE4dEr}
```

## 50 - Easy 2 - Web

> Plan ahead.
>
> Heighten your situational awareness.
>
> Dress in clothes that are common to the area where you are, or where you plan to go.
>
> Avoid bright colors. Neutral will blend in. Choose grey over black or white or red.
>
> easy2.web.sunshinectf.org

There is a form:

```html
   <form action="index.php" method="post">
        Username: <input type="text" name="username" value="admin" /> <br />
        Password: <input type="password" name="password" value="admin" /> <br />
        <input type="hidden" name="is_admin" value="0" />
        <input type="submit" name="" value="submit" />
    </form>
```

Change the hidden input field value (`<input type="hidden" name="is_admin" value="1" />`) from `0` to `1` and submit inputs.

We get the flag: `sun{n07HIn9_C4N_83_HiDD3n_Fr0m_4_h4X0R}`.

## 150 - Tag - Forensics

> You are surrounded by zombies on all sides. Blood and flesh that drip from their mouths. You run for cover in an abandoned building. As you huddle in terror under a delapitated desk your foot brushes against a small package. What could be inside???
>
> https://sunshinectf.org/files/88f6331e28192d47e3842911b21057f0/tag.gz

```
$ file tag.gz
tag.gz: pcap-ng capture file - version 1.0
```

So open `tag.gz` with Wireshark.

We can be ready to find some files:

```
$ binwalk tag.gz

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
1181          0x49D           JPEG image data, EXIF standard
1193          0x4A9           TIFF image data, big-endian, offset of first image directory: 8
14252         0x37AC          Unix path: /www.w3.org/1999/02/22-rdf-syntax-ns#"> <rdf:Description rdf:about="" xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmlns:dc="http://
18412         0x47EC          Copyright string: "Copyright (c) 1998 Hewlett-Packard Company"
244133        0x3B9A5         JPEG image data, JFIF standard 1.01
396028        0x60AFC         7-zip archive data, version 0.3
```

In Wireshark: File > Export Objects > HTTP. Here we can save 3 files: 2 images (jpeg) and 1 archive (7z).

Images are useless and archive is password protected.

Let's find password in the `pcap-ng`. If you do a `strings` on `tag.gz` you can see a lot of trailing data that are no port of the extracted files.

The data are on frame n°313 in the TCP data field (length 1116). Right click on data and *Export Selected Packet Bytes*.

That looks like base64:

```
$ cat data.ext
NDg2NTIwNzc2MTZjNmI2NTY0MjA2Zjc1NzQyMDY5NmUyMDc0Njg2NTIwNjc3MjYxNzkyMDZjNjk2NzY4NzQyMDYxNmU2NDIwNzM3NDZmNmY2NDIwNjE2ZTY0MjA2ODY1MjA3MzYxNzcyMDY2NmY3MjIwNjEyMDYyNzI2OTY1NjYyMDZkNmY2ZDY1NmU3NDIwNzQ2ODY1MjA2MTYyNzM2ZjZjNzU3NDY1MjA3NDcyNzU3NDY4MjA2ZjY2MjA3NDY4NjUyMDc3NmY3MjZjNjQyZTIwNTQ2ODY1MjA2MzZmNmM2NDIwNzI2NTZjNjU2ZTc0NmM2NTczNzMyMDYzNjk3MjYzNmM2OTZlNjcyMDZmNjYyMDc0Njg2NTIwNjk2ZTc0NjU3Mzc0NjE3NDY1MjA2NTYxNzI3NDY4MmUyMDQ0NjE3MjZiNmU2NTczNzMyMDY5NmQ3MDZjNjE2MzYxNjI2YzY1MmUyMDU0Njg2NTIwNjI2YzY5NmU2NDIwNjQ2ZjY3NzMyMDZmNjYyMDc0Njg2NTIwNzM3NTZlMjA2OTZlMjA3NDY4NjU2OTcyMjA3Mjc1NmU2ZTY5NmU2NzJlMjA1NDY4NjUyMDYzNzI3NTczNjg2OTZlNjcyMDYyNmM2MTYzNmIyMDc2NjE2Mzc1NzU2ZDIwNmY2NjIwNzQ2ODY1MjA3NTZlNjk3NjY1NzI3MzY1MmUyMDQxNmU2NDIwNzM2ZjZkNjU3NzY4NjU3MjY1MjA3NDc3NmYyMDY4NzU2ZTc0NjU2NDIwNjE2ZTY5NmQ2MTZjNzMyMDc0NzI2NTZkNjI2YzY5NmU2NzIwNmM2OTZiNjUyMDY3NzI2Zjc1NmU2NDJkNjY2Zjc4NjU3MzIwNjk2ZTIwNzQ2ODY1Njk3MjIwNjM2Zjc2NjU3MjJlMjA0MjZmNzI3MjZmNzc2NTY0MjA3NDY5NmQ2NTIwNjE2ZTY0MjA2MjZmNzI3MjZmNzc2NTY0MjA3NzZmNzI2YzY0MjA2MTZlNjQyMDYyNmY3MjcyNmY3NzY1NjQyMDY1Nzk2NTczMjA3NzY5NzQ2ODIwNzc2ODY5NjM2ODIwNzQ2ZjIwNzM2ZjcyNzI2Zjc3MjA2OTc0MmU=

$ cat data.ext | base64 -di
48652077616c6b6564206f757420696e207468652067726179206c6967687420616e642073746f6f6420616e642068652073617720666f722061206272696566206d6f6d656e7420746865206162736f6c757465207472757468206f662074686520776f726c642e2054686520636f6c642072656c656e746c65737320636972636c696e67206f662074686520696e746573746174652065617274682e204461726b6e65737320696d706c616361626c652e2054686520626c696e6420646f6773206f66207468652073756e20696e2074686569722072756e6e696e672e20546865206372757368696e6720626c61636b2076616375756d206f662074686520756e6976657273652e20416e6420736f6d6577686572652074776f2068756e74656420616e696d616c73207472656d626c696e67206c696b652067726f756e642d666f78657320696e20746865697220636f7665722e20426f72726f7765642074696d6520616e6420626f72726f77656420776f726c6420616e6420626f72726f7765642065796573207769746820776869636820746f20736f72726f772069742e
```

That now looks like hex:

```
$ cat data.ext | base64 -di | xxd -r -p
He walked out in the gray light and stood and he saw for a brief moment the absolute truth of the world. The cold relentless circling of the intestate earth. Darkness implacable. The blind dogs of the sun in their running. The crushing black vacuum of the universe. And somewhere two hunted animals trembling like ground-foxes in their cover. Borrowed time and borrowed world and borrowed eyes with which to sorrow it.
```

Now we get a quote, let's paste it in a search engine, that come from `The Road`.

Extract `flag.7z` with `the road` as password.

```
$ cat flag.txt
sun{phUn_1n_7h3_c0MPl373_l4ck_0f_5UN}
```

## 150 - Vanity - Crypto

> You need to buy some things, and luckily the local merchant accepts Bitcoin! You found out he only checks that the first four characters of his address and the one you send the coins to. Can you make him think you are sending him the coins while they really go to yourself?
>
> nc pwn.sunshinectf.org 40003

We need to have a bitcoin address where the 4 first char are the same as the vendor address:

```
$ nc pwn.sunshinectf.org 40003
The local vendor is warry to accept anynthing but bitcoin, but he only checks the first
four characters of each address.
Can you make him think you are sending him the coins while they really go to yourself?






Hey there! Here's my Bitcoin address:
1kXeFFwxJcyVbHVUkJ59ykfax4F4P4NoZb

Enter your private key(in hex, uncompressed):
fc3009f6a11328a4ece34ee4209c712f7f14a81fd0ce4d0b0e8dbb9f40d9d7d9

That key came out to:
1kXer5xriQaRBWPZWLUyQuhy7WFMbiQAi

Glad the payment went through, here are your goods.
You only have 4 purchases left!
Hey there! Here's my Bitcoin address:
1tM15GpBJbAc8uc2MPz2qsnSMBs1k2VA7H

Enter your private key(in hex, uncompressed):
cb14680d253636f95dd68e7cab26773ffb8ceba4e2aa1848668cee1dd044706b

That key came out to:
1tM1BtrZ26tzDjWyXGgyUHPT1b5WJCqz3

Glad the payment went through, here are your goods.
You only have 3 purchases left!
Hey there! Here's my Bitcoin address:
1RMogK6nrk7LLkFGBBdcRjvozC1rs9GDSp

Enter your private key(in hex, uncompressed):
92ac39806ed9b333004a405042b0598a6bdedfed5e5e262e737b70f66921729e

That key came out to:
1RMo33Retq4GrcebJKEChHLQZZFCpR22q

Glad the payment went through, here are your goods.
You only have 2 purchases left!
Hey there! Here's my Bitcoin address:
1EdVCpLJ1aSQqoKzWy5T4LTMmKuqbtzoyF

Enter your private key(in hex, uncompressed):
1821075f9b31112a5543703a7a2b7bbdc0553265ce302b3daafd3e77f0d0282f

That key came out to:
1EdVGodXWjtPn4JsvqEDocZq4d3B4Qv2pL

Glad the payment went through, here are your goods.
You only have 1 purchases left!
Hey there! Here's my Bitcoin address:
1xRWfB5a9S5q74fE2w7QtS4GhghBtLeP7j

Enter your private key(in hex, uncompressed):
6fa7d622cd209657c1d858c546d4aa31d525a9167e33e7bbbc38944ba0cee71e

That key came out to:
1xRWUawaT6KsjaSzgxUyEnFTByqpdLm5C

Thanks for all of your payments, here's your flag:
sun{a1waY5_ch3k__th0se_bits}
```

To generate a private key matching a 4 case-sensitive pattern is used [vanitygen](https://github.com/samr7/vanitygen):

```
$ ./vanitygen 1kXe
Difficulty: 4553521
Pattern: 1kXe
Address: 1kXer5xriQaRBWPZWLUyQuhy7WFMbiQAi
Privkey: 5KjMNoAXxtRffAz96EzguPq8ZDAJZUrHmZ9ddCztA68QoD4kP45

$ ./vanitygen 1tM1
Difficulty: 4553521
Pattern: 1tM1
Address: 1tM1BtrZ26tzDjWyXGgyUHPT1b5WJCqz3
Privkey: 5KMiyog4NfQmJ2ZFxsaLkVNVLaMGy3uMpwTX8QEgQSEBwkTcrgT

$ ./vanitygen 1RMo
Difficulty: 4553521
Pattern: 1RMo
Address: 1RMo33Retq4GrcebJKEChHLQZZFCpR22q
Privkey: 5Jvt95bYNH7E9scFXSidFrSN9TkCdHiHBTAqPrh89S1cexJVgGi

$ ./vanitygen 1EdV
Difficulty: 77178
Pattern: 1EdV
Address: 1EdVGodXWjtPn4JsvqEDocZq4d3B4Qv2pL
Privkey: 5HzuvbeUwNxixqAnK2UPfjRQVkgEe7coKBLmZGaBPifDR2gycL6

$ ./vanitygen 1xRW
Difficulty: 4553521
Pattern: 1xRW
Address: 1xRWUawaT6KsjaSzgxUyEnFTByqpdLm5C
Privkey: 5JfTg4Pw3n2i81bK8keWb9R3gnRDaNi8p1ZJ61Lzo79nnhuPh4A
```

But output of the private key is WIF encoded in base58 and we need a hexadecimal encoding of the uncompressed private key, so I used [bitcoin-tool](https://github.com/matja/bitcoin-tool) to convert it:

```
$ ./bitcoin-tool --input-type private-key-wif --input-format base58check --output-type private-key --output-format hex --input 5KjMNoAXxtRffAz96EzguPq8ZDAJZUrHmZ9ddCztA68QoD4kP45
fc3009f6a11328a4ece34ee4209c712f7f14a81fd0ce4d0b0e8dbb9f40d9d7d9

$ ./bitcoin-tool --input-type private-key-wif --input-format base58check --output-type private-key --output-format hex --input 5KMiyog4NfQmJ2ZFxsaLkVNVLaMGy3uMpwTX8QEgQSEBwkTcrgT
cb14680d253636f95dd68e7cab26773ffb8ceba4e2aa1848668cee1dd044706b

$ ./bitcoin-tool --input-type private-key-wif --input-format base58check --output-type private-key --output-format hex --input 5Jvt95bYNH7E9scFXSidFrSN9TkCdHiHBTAqPrh89S1cexJVgGi
92ac39806ed9b333004a405042b0598a6bdedfed5e5e262e737b70f66921729e

$ ./bitcoin-tool --input-type private-key-wif --input-format base58check --output-type private-key --output-format hex --input 5HzuvbeUwNxixqAnK2UPfjRQVkgEe7coKBLmZGaBPifDR2gycL6
1821075f9b31112a5543703a7a2b7bbdc0553265ce302b3daafd3e77f0d0282f

$ ./bitcoin-tool --input-type private-key-wif --input-format base58check --output-type private-key --output-format hex --input 5JfTg4Pw3n2i81bK8keWb9R3gnRDaNi8p1ZJ61Lzo79nnhuPh4A
6fa7d622cd209657c1d858c546d4aa31d525a9167e33e7bbbc38944ba0cee71e
```

## 150 - Zombiedex - Web

> zombiedex.web.sunshinectf.org

**Warning**: partial solution!

Convert from HEX to ASCII:

```bash
$ printf %s '66534a68496a6f695a484a7664334e7a595841694c434a756157316b59534936496d5674595735795a584e31496e733d' | xxd -r -p
fSJhIjoiZHJvd3NzYXAiLCJuaW1kYSI6ImVtYW5yZXN1Ins=
```

Base64 decode:

```bash
$ printf %s 'fSJhIjoiZHJvd3NzYXAiLCJuaW1kYSI6ImVtYW5yZXN1Ins=' | base64 -d
}"a":"drowssap","nimda":"emanresu"{
```

Reverse the string:

```bash
$ printf %s '}"a":"drowssap","nimda":"emanresu"{' | rev
{"username":"admin","password":"a"}
```

That's the credentials I used to lokin in JSON format.

Or in one line:

```bash
printf %s '66534a68496a6f695a484a7664334e7a595841694c434a756157316b59534936496d5674595735795a584e31496e733d' | xxd -r -p | base64 -d | rev
{"username":"admin","password":"a"}
```

Let's inject something in the JSON and send a crafted cookie.

**Warning**: we didn't go further.
