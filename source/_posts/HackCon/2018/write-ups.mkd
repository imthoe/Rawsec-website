---
layout: post
title: "HackCon 2018 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - stegano
  - crypto
date: 2018/08/17
updated: 2018/08/27
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By              | Version | Comment
| ---             | ---     | ---
| noraj and imth  | 1.0     | Creation

### CTF

- **Name** : HackCon 2018
- **Website** : [hackcon.in](http://www.hackcon.in/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/652)

## 10 - Caesar Salad - Crypto

> Can you toss Caesar's salad?
>
> `q4ex{t1g_thq_p4rf4e}p0qr`

My ruby caesar cipher solver:

```ruby
#!/usr/bin/env ruby

# from https://gist.github.com/matugm/db363c7131e6af27716c
def caesar_cipher(string, shift = 1)
  alphabet   = Array('a'..'z')
  encrypter  = Hash[alphabet.zip(alphabet.rotate(shift))]
  # " " => c because I don't want to void non-letters chars
  string.chars.map { |c| encrypter.fetch(c, c) }
end

text = "q4ex{t1g_thq_p4rf4e}p0qr"
text.downcase! # put lowercase

(1..25).each do |i|
  puts "#{i}: " + caesar_cipher(text, i).join + "\n\n"
end
```

Answer was:

```
13: d4rk{g1t_gud_c4es4r}c0de
```

## 20 - Hoaxes and Hexes - Stegano

> My friend sent me a meme and told me to 'look at it differently'. Tf?

Just check if there is trailing data after the picture.

```
$ strings maymays.jpg | tail -1
d4rk{1m_d0wn_h3r3}c0de
```

## 50 - Twins - Stegano

> Two brothers separated long ago, finally decide to reunite
>
>    Each one holds the secret key, but needs the other brother to read it.
>
> Can you find their hidden secret?

We have two text files doing the same length:

```
$ cat tmp/file1 
it>J1%}F(0)9o4}-hld<xY3P3s,kcJx9!ajA|_XXgOl34gDOJ5thx[JewFa8rJP@iq_>$]Ucgw#}1I*GZv4jR5VyJ7^Z#KWQv9%*<Nk^$>ZuT%<LpeWyk}&o((OH1KqDG)d]Q1Ss}C0[5}r}E<-DS.E6^{{SmR9Sell%iZvKxdc-w-Z?ouij_t91S_e^<U|5EF.&tEWRY8U1WA)GpV9ZlV[W)TAI9#vqte@*?uHbTwML0}xxB[k^,!!2C{%C8<>_uW7p6dYI10M8_K$hcfM@tdpJ^xm9R0<I6c](]4>&oy.?*u{qbNRB50(,)I$fcRSO<8kU.}@J#Ii,K_$vj?LYZhfVdD8f8ZepnFXV%Q.^C<vdiWuOR3ps5b_WobBb7AL$!}q8H]$OwEjk<Rj^yno*5BZK00f.nTgY&.tE*%0?cnXhN*vJZiyhLAu7N%6M0s%mHgGWkhDcJ7jTe<F$-_Z]vXEJ(<c9W^ug]l*6Rgj^k^D)EFb),B?YAGngkwJ?4_FQ{&p0Nn&]OgCB#&)a,oM>_7hOSASpW-24c(8(AKce26oZBbBj(Thz.{i7zb{vwzGfSV>JsW,C&ikBYHC%0TpP0MNXH5?@uq&jTA_t$@u?BJXa?!I_UCdf}]ql?.XH$e9vc)tQ4|hMazm?-n*2aiv<Y!-k<Xcx[4K2Grk!I-Bt<CMc)f|B8VrCQFLK}^wk$2E_V5,.Yh}_v{9&T$4tpyz,c#1TSeQ4^QvrZr{NX@e&Ia]$7(78FFGdz)W-7[<Tdy0WpT0f)*AWnK|KggNP?V[QkL({tYi0&WIUZuU!q?1x%JhLu.U}Y!CaZoMY8eiYA-48R-sxq<d%cVG*dTX$u3{?I!Ji?FrW?yy_(Og^}dIx((ahiB$-_SHB$xc^y>#3ab|^|L8D1bnBQkbR59h&v0Qtjxz.ku*d,QP%[aM(%i6oneh4Y1&7dURk${LU^fzfUrcBj3{-Lk4!$I^F9&ajVAq9]{,7C?T]#?7?xR{?,f,zTt4]D&$UX3gH(pMskTH#Dsv6Hxc9OCdlq3@EV9H3)&&N5rzZm8?5Ssp#v0RH@hkwM7Y}i}K@DFDsB36bxKuvF}-W2|1LfZ@t{N#bf^af%k([i<uT9KtHy65Qu?L&x1YQ3PDs.*%6-#MrT{G%ewa^Xl,R02zHWXc0AF,cAfr_)n1jgrLy1}KS}!*(RZAhyoed?2!FK0^q@x|b^Dn4]&5?$UwXxfG@fB5Ut){lH9FMb)xUc@tq?l#PlD?J>r_rDSJ}*!@,?,>k?[ng5[sIxRM*cNTtmc4sm-{_HA3bi*if&cs4-H*SwxAUur8B_m)E}!tb&3m0*Oz)-U]s,nI?WeCD>3qhR)30I*7&b>tXolna?D-bAhFRcb<B<lR@n0hFp#ll?0AS{ayF8WH|x]%6P@[V(TPt?_6.5J&[q3$-6%I.ApPp|(,YsUg{<2x9HZ@{t,eZ*Bi<*-Y5bmeLY]x6o2i4@4$Z,rJ^!PKG<_sSLOs8OB]U}]B<!}AIaAYR?[qQ|Eiy6$>|euD(**cC((C,*fnY{Q&6C)ndJzVwb*-8p8P8Q|20JAm5glUa!3s?b-c!)!Ps]n.ei>80Ufnz0]?v|%}kcQoS^)N(Wmddr3(a|}9Q[BgaaqWF0EZa%7e.?dp7S0j4dLFbUq}7kIio47BO}UU?GPVv?j]&-5g5[x7zeYC%,)&BiI>e

$ cat tmp/file2 
ck<kfsk^e^s6YuPBLZdP8#G(LzNIoqiZc&6J0}eWeRZm4D>-2gi.g2{q?qb*rMFCjZQdJ}y}9xmDDgHS-a$b57GGX&mlw,_FT_c<{{|ZvQg|L7fs16b-k6^-%U197%>!wnV$1$^@>9.69mQ7w.Gz[Sd7?j{ndQZGG^l(jxS*S<XuD|j*oBFfWUFW5LLkMBaOlS(hk9pB8k0hk?w@{LvUZlEK!JxB}?<,hw4f<_4rL|O40?$drx?p}1)i|K91k1AYn4^bO>QWbF2z>?q5t#D)ss&G&7]T%.DxB,5$j}DJQ9?^pa3*@O}p,F]>bG|Q?0}zclk^o3{8N)x26LQuD<En<>[[z<f@X$b1et-LA,]4L5?Di|&^e-6)LK#^eURAO?MYpgo0P4A40SDvKF6(^oQvgv?_of93<SU6_]pl?aQh<n#6T<kOD3JvF<G>hZ!VQ4p]p(R?aKHNI5MJj^i[ztk?3W3b[qp)5Ow>50w[!gRSIpQp)p[?1dZgg8D,ljR@%_PZVqpz(2PMmG|-h]x?<L{Ia!77xudwxv$4Dy(V?m6I@7&_G7mk@f5&e5e*c,G&9k2c?w2bs}]-HM<N3&Y*vhP]Sdw00$ysj$T{huafsU?jsjoY5HM8q]$4X?d76w#p>#NCOMt%DxKZ?AVryt0i8<M@*>8LEQ.M)zT(Rzaj0iWa2(o>[^WG_&>pquO.AwzU_}c28_oREcI_aw3CD6#KUoHEerp#ux%PRX93veBc-2?67N9?u8y?JU?Fr3i{nG?0b!0LvpUNQdIPuI}NsIgfStiF?qmj&6Lkcbu,tp4DR[T[D8x@k-SGETB}s$lJ?!t$3yw*}BSA1rOl<mIs95N%uZ6*XeYYsRrSpP-_Y4uDFd696K!^P8>5vBX].I!n[qW2}?YIZUnpVovNSu,b(FzsXJHZ*jQKgbCm@g%xUd%$aVwxV#8S[1,Regb,L9!BKeSfbZ119DZv#f9S{7_7n,JyJq(74h6iOc||zj>r,lLTvf&mrOY).ApRWyott^QxEz#!KR]KUg687J&M&fAZfLL(n3X3K]%uVMW7sk7)@1-l*FUeO|hRd[%tBE(be3c)mF2E6,NY-yM_SLjy?zb]I|pQg[7$$[Z4U?Q<oykbYiFYWn.#PMJQYmLtu,gr?L1Mg4_p3Hg_?}d_<?sMHwq6qUTE,[FB*nGz&.TswPU{F?Io3$eSR?i4uC_r0Njv.)ndak3vy)llng2>>517[JiM3%AskwIqd^M$prsYx{QnG*n^a]4a$FX4Px4T9Vy!XwEW?wurcw.,hP)t[3e%C!,%-SQ]$ablGWqgzP^&M?m0<J2U,Me5xb]sfyfBTj6L8g<rI8(vQUlly.3{re#oGkJqvZECueyAbxHt<dNc?tn2C*D6-8<5H1-u|5v5sLVGZI*v8lY8AWZ--G9Rw.%LVI[efs2D,QE7cP*Qs*.@1a3}e9VyL!3hkWfSJ0RI7qKZqICY|0@OjMebt6>0QQDoCZxE&,}3%47$}H>ZpN^(txBN9b80niStA20K94q]pk7!.HikeY-5Icmm(hiT^(ClsoINWqxcemNvQ.i|Xv)kAwGz{Uca{*Y&CmiY?vb(CROd01aKisjo([cyhE1b7^Pdwbyd_tU}*kHi7SPCC<US)EjE%K!c?p7fb!5yT]DR}i1}.<b>mK1GA]5L.?nNsQz[b1A46bV^U?i5Olke
```

First we tried to xor them but we had nothing in output. So OddCoder had an idea: keeping only identical character at a same index.

He made a python script but for fun I decided to make a shorter and cleaner ruby script:

```ruby
file1 = File.read('tmp/file1')
file2 = File.read('tmp/file2')

puts file1.each_char.zip(file2.each_char).select { |a,b| a == b }
```

Flag was `d4rk{lo0king_p4st_0ur_d1ff3renc3s}c0de`.

## 30 - Diversity - Crypto

> So much diversity, my mind boggles..!

We were given a text with numbers in different bases, lets convert them to their ASCII representatives.

```python
import binascii

text = "b1001000 x69 d33 d32 o127 b1100101 o154 o143 b1101111 o155 o145 d32 o164 d111 d32 x48 b1100001 x63 o153 b1000011 o157 x6e d39 o61 b111000 x2c d32 d111 b1110010 d103 d97 x6e o151 x73 d101 d100 o40 d97 b1110011 b100000 x70 o141 o162 x74 d32 x6f x66 b100000 o105 b1110011 x79 b1100001 d39 d49 b111000 x20 b1100010 d121 b100000 x49 o111 b1001001 x54 b100000 b1000100 x65 x6c o150 x69 b101110 x20 o111 d110 b100000 o143 d97 d115 o145 o40 b1111001 b1101111 x75 b100111 x72 x65 x20 x73 x65 b1100101 b1101011 x69 o156 x67 d32 b1100001 o40 o162 x65 o167 b1100001 o162 o144 d32 x66 d111 x72 b100000 o171 x6f d117 b1110010 o40 d101 x66 x66 x6f x72 d116 o163 x2c b100000 d104 b1100101 d114 o145 x27 d115 x20 b1100001 d32 d102 d108 b1100001 x67 x20 x3a b100000 o144 x34 o162 x6b x7b o151 d95 d87 o151 x73 b100011 d95 x41 o61 x6c d95 b1110100 d52 d115 b1101011 d53 o137 o167 x33 d114 o63 o137 d116 b1101000 o151 o65 x5f x33 d52 o65 o171 o137 x58 b1000100 b1000100 b1111101 x63 d48 d100 d101 d46 b100000 o101 x6e b1111001 d119 b1100001 b1111001 x73 b101100 x20 o150 d111 b1110000 b1100101 o40 x79 o157 d117 b100000 b1101000 o141 x76 x65 b100000 d97 x20 o147 d111 b1101111 d100 b100000 b1110100 b1101001 d109 b1100101 d32 x3b x29".split(' ')

solution = ''
for x in text:
    if x[0] == 'b': #binary
        solution += chr(int(x[1:],2))
    elif x[0] == 'x': # hexadecimal
        solution += x[1:].decode("hex")
    elif x[0] == 'd': # decimal
        solution += chr(int(x[1:]))
    elif x[0] == 'o': # octal
        solution += chr(int(x[1:],8))

print(solution)
```

Flag: `d4rk{i_Wis#_A1l_t4sk5_w3r3_thi5_345y_XDD}c0de`
