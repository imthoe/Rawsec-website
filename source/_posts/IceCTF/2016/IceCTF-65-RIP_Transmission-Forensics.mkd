---
layout: post
title: "IceCTF - 65 - R.I.P Transmission - Forensics"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - forensics
date: 2016/08/25
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : IceCTF 2016
- **Website** : [https://icec.tf/](https://icec.tf/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/319)

### Description

[This][this] seems to be recieving some sort of transmission. Our experts have been working around the clock trying and figure out what the hell it means with no hope of getting to the bottom of it. You're our only hope.
[this]:https://play.icec.tf/problem-static/rip_2067f9686b4d07eea2cac19b9c6588b2abac16500135901ce8781e4ccc262446

## Solution

1. Check file type:
```
file rip
rip: ELF 32-bit LSB executable, Intel 80386, version 1 (GNU/Linux), statically linked, for GNU/Linux 2.6.32, BuildID[sha1]=9fd51896db8e1b47260951e8f6ca7d6023ce9ae6, not stripped
```
2. Execute it: [http://asciinema.org/a/276ngyyh05z80luxnqbyy95mq](http://asciinema.org/a/276ngyyh05z80luxnqbyy95mq)

3. That's defintly not what we're looking for but only a lapse in concentration.
4. Check what is hidden in this binary:
```
binwalk rip

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             ELF, 32-bit LSB executable, Intel 80386, version 1 (GNU/Linux)
993400        0xF2878         Unix path: /usr/lib/locale/locale-archive
1014524       0xF7AFC         Unix path: /proc/sys/vm/overcommit_memory
1024257       0xFA101         Unix path: /proc/sys/kernel/rtsig-max
1025342       0xFA53E         Unix path: /sysdeps/unix/sysv/linux/getcwd.c
1027000       0xFABB8         Unix path: /proc/sys/kernel/osrelease
1093862       0x10B0E6        Unix path: /nptl/sysdeps/unix/sysv/linux/i386/../fork.c
1097017       0x10BD39        ELF, 32-bit LSB no file type, (SYSV)
1100142       0x10C96E        Unix path: /sysdeps/unix/sysv/linux/dl-origin.c
1323949       0x1433AD        Zip archive data, encrypted at least v2.0 to extract, compressed size: 112199, uncompressed size: 112190, name: rip.jpg
1436306       0x15EA92        End of Zip archive
```
5. We can see there is a zip with encrypted data (protected by password). Firstly we can extract this zip from the binary:
```
foremost -t zip -i rip -T -v
Foremost version 1.5.7 by Jesse Kornblum, Kris Kendall, and Nick Mikus
Audit File

Foremost started at Sat Aug 20 23:01:53 2016
Invocation: foremost -t zip -i rip -T -v
Output directory: /root/CTF/IceCTF/2016/65-RIP_Transmission-Forensics/output_Sat_Aug_20_23_01_53_2016
Configuration file: /etc/foremost.conf
Processing: rip
|------------------------------------------------------------------
File: rip
Start: Sat Aug 20 23:01:53 2016
Length: 1 MB (1698648 bytes)

Num	 Name (bs=512)	       Size	 File Offset	 Comment

foundat=rip.jpgUT
0:	00002585.zip 	     109 KB 	    1323949 	 
*|
Finish: Sat Aug 20 23:01:53 2016

1 FILES EXTRACTED

zip:= 1
------------------------------------------------------------------

Foremost finished at Sat Aug 20 23:01:53 2016
```
6. Secondly we can bruteforce the password with [patator][patator], I first tried with [500-worst-passwords][500-worst-passwords] dict and then I was successful with [john][john] dict:
```
patator unzip_pass zipfile=output_Sat_Aug_20_23_01_53_2016/zip/00002585.zip password=FILE0 0=john.txt -x ignore:code!=0
23:22:46 patator    INFO - Starting Patator v0.7-beta (https://github.com/lanjelot/patator) at 2016-08-20 23:22 CEST
23:22:46 patator    INFO -                                                                              
23:22:46 patator    INFO - code  size    time | candidate                          |   num | mesg
23:22:46 patator    INFO - -----------------------------------------------------------------------------
23:22:49 patator    INFO - 0     142    0.020 | bunny                              |   966 | No errors detected in compressed data of /root/CTF/IceCTF/2016/65-RIP_Transmission-Forensics/output_Sat_Aug_20_23_01_53_2016/zip/00002585.zip.
23:22:56 patator    INFO - Hits/Done/Skip/Fail/Size: 1/3107/0/0/3107, Avg: 321 r/s, Time: 0h 0m 9s
```
7. Now we have just to `unzip output_Sat_Aug_20_23_01_53_2016/zip/00002585.zip` with `bunny` as password.
8. And `display output_Sat_Aug_20_23_01_53_2016/zip/rip.jpg` to get the flag:  `IceCTF{1_Lik3_7o_r1P_4nD_diP_411_7He_ziP5}`

[patator]:https://github.com/lanjelot/patator
[500-worst-passwords]:http://downloads.skullsecurity.org/passwords/500-worst-passwords.txt.bz2
[john]:http://downloads.skullsecurity.org/passwords/john.txt.bz2
