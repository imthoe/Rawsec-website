## TEAM

**Rawsec** was originally a French security CTF team but is now International because people from all around the world joined us. It was created in January 2016 by **noraj**. We frequently participate in both online and on site security Capture The Flag competitions, publish write-ups on CTF tasks.

More details about [what is a CTF](#ctf).

Rawsec currently has:

+ Active members:

Pseudo              | Position                                | Description                         | CTFTime profile                         | Posts on the blog
---                 | ---                                     | ---                                 | ---                                     | ---
**noraj**           | [Captain]                               | Cyber-security engineer & Pentester | [link](https://ctftime.org/user/16344)  | [link](https://rawsec.ml/author/noraj/)
**OddCoder**        | [Forensic Quarterback]                  | Freelance Software engineer         | [link](https://ctftime.org/user/43768)  | [link](https://rawsec.ml/author/oddcoder/)
**highsenburger69** | [ROP-a-dope]                            | High School Student Script Kiddie   | [link](https://ctftime.org/user/38216)  |
**imth**            | [Esoteric Programming Language Punter]  | Computer Science Student            | [link](https://ctftime.org/user/39106)  | [link](https://rawsec.ml/author/imth/)
**pxmme**           | [Web Offensive guard]                   | CTF enthusiast                      |                                         | [link](https://rawsec.ml/author/pomme/)
**fuzzer**          | [Pwn Offensive tackle]                  | CTF enthusiast                      | [link](https://ctftime.org/user/31563)  |

+ Several part-time players, see the [ctftime team page](https://ctftime.org/team/27232)
+ Former active members:
  - **Aikari** - [Striker n°9] - Student Cyber-security engineer - [CTFTime profile](https://ctftime.org/user/28668)

Feel free to ask for joining the team, we are looking for active members. We are available on [Discord](https://discord.gg/xvTb2vx). You can also try CTFTime private message or [Twitter](https://twitter.com/rawsec_cyber).

## Blog

**Rawsec** is also a blog talking about IT and news but it is more focus on security and linux. The blog is also hosting *Rawsec*'s (CTF team) write-ups. The blog is managed by **noraj** (*Alexandre ZANNI*).

## CTF

In Cyber-Security, **Capture the Flag (CTF)** is a special kind of information security competitions. There are two main types of CTFs: Jeopardy and Attack-Defence.

CTF contests are usually designed to serve as an educational exercise to give participants experience in securing a machine, conducting and reacting to the sort of attacks found in the real world or improve their pentesting skills.

Jeopardy-style CTFs has a couple of questions (tasks/challenges) in range of categories. For example, Web Exploitation, Forensics, Cryptography, Binary Exploitation or something else. Team can gain some points for every solved task. More points for more complicated tasks usually. In some CTFs, the next task in chain can be opened only after some team solve previous task. Then the game time is over, sum of points shows you a CTF winner.

Attack-defence or Wargame is another interesting kind of competitions. Here every team has own network (or only one host) with vulnarable services. Your team has time for patching your services and developing exploits usually. So, then organizers connects participants of competition and the wargame starts! You should protect own services for defence points and hack opponents for attack points. Historically this is a first type of CTFs, everybody knows about DEF CON CTF - something like a World Cup of all other competitions.

CTF games often touch on many other aspects of information security: cryptography, steganography, binary analysis, reverse engineering, mobile security, programming and others. Good teams generally have strong skills and experience in all these issues.

The tracking of the CTF's is often done in the website ctftime.org and write-ups will be written by the players (and sometimes organizers) and will be hosted on team's blog or git repository.

Sources: *[CTFTime][ctftime]* - *Wikipedia [1][wikipedia1] and [2][wikipedia2]*

[ctftime]:https://ctftime.org/ctf-wtf/
[wikipedia1]:https://en.wikipedia.org/wiki/Capture_the_flag#Computer_security
[wikipedia2]:https://en.wikipedia.org/wiki/Wargame_(hacking)
